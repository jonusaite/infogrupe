<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Kompiuterinės technikos taisymo sutarčių registracijos sistema">
    <meta name="copyright" content="Aistė Jonušaitė">
    <meta name="author" content="Aistė Jonušaitė">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    <nav>
        <div class="navbar navbar-default collapse navbar-collapse">
            <!-- Branding Image -->
            <div class="navbar-brand">
                {{ config('app.name', 'Laravel') }}
            </div>
        </div>

        <!-- Sidebar menu -->
        <div class="navbar navbar-default col-sm-3">
            <!-- Collapsed Hamburger -->
            <div class="navbar-header">

                <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>

            <!-- Main menu -->
            <ul class="nav nav-pills nav-stacked collapse navbar-collapse" id="app-navbar-collapse">
                <li><a class="text-muted" href="{{ url('/') }}">Pradžia</a></li>
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a class="text-muted" href="{{ route('login') }}">Prisijungti</a></li>
                    <li><a class="text-muted" href="{{ route('register') }}">Registruotis</a></li>
                @else
                    <li><a class="text-muted" href="{{ route('customers') }}">Užsakovai</a></li>
                    <li><a class="text-muted" href="{{ route('order') }}">Nauja sutartis</a></li>
                    <li><a class="text-muted" href="{{ route('orders') }}">Sutartys</a></li>
                    <li><a class="text-muted" href="{{ route('details') }}">Rekvizitai</a></li>
                    <li><a class="text-muted" href="{{ route('contract') }}">Sutarties aprašymas</a></li>
                    <li><a class="text-muted" href="{{ route('profile') }}">Profilis</a></li>
                    <li><a class="text-muted" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">Atsijungti
                            ({{ Auth::user()->username }})</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endif
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
