@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">Pridėti užsakovą</div>

            <div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('saveCustomer', $customer->id) }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('customer') ? ' has-error' : '' }}">
                        <label for="customer" class="col-md-4 control-label">Užsakovas</label>

                        <div class="col-md-6">
                            <input id="customer" type="text" class="form-control" name="customer"
                                   value="{{ $customer->customer }}" required autofocus>

                            @if ($errors->has('customer'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('customer') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-4 control-label">Telefonas</label>

                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control" name="phone"
                                   value="{{ $customer->phone }}" required autofocus>

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">El. pašto adresas</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $customer->email }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Išsaugoti
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection
