@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">Sutarties redagavimas</div>

            <div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('editContract') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('contract') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                        <textarea name="contract" class="form-control fixed-width" rows="10" id="contract" required>{{ old('contract') ? old('contract') : (($contract) ? $contract->contract : '') }}</textarea>

                            @if ($errors->has('contract'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('contract') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Išsaugoti
                            </button>
                        </div>
                    </div>

                </form>

            </div>

        </div>
    </div>

@endsection
