@extends('layouts.app')

@section('content')

    <div class="col-sm-9 hidden-print">

        <div class="panel panel-default">
            <div class="panel-body table-responsive">

                <p class="panel-heading text-center">
                    Kompiuterinės technikos testavimo / taisymo sutartis
                    Nr. {{ str_pad($order->id, 5, '0', STR_PAD_LEFT) }}
                    <br>
                    Priėmimo data: {{ date('Y-m-d', strtotime($order->created_at)) }}
                </p>

                <table class="table">

                    <thead>
                    <tr>
                        <th class="col-xs-6">Vykdytojas</th>
                        <th class="col-xs-6">Užsakovas</th>
                    </tr>
                    </thead>

                    <tbody>

                    <tr>

                        <td>
                            {{ $order->details->provider }} <br>
                            <strong>Adresas:</strong> {{ $order->details->address }} <br>
                            <strong>Telefonas:</strong> {{ $order->details->phone }} <br>
                            <strong>El. paštas:</strong> {{ $order->details->email }} <br>
                            <strong>Darbo laikas:</strong> {{ $order->details->hours }}
                        </td>

                        <td>
                            {{ $order->customer->customer }} <br>
                            <strong>Telefonas:</strong> {{ $order->customer->phone }} <br>
                            <strong>El. paštas:</strong> {{ $order->customer->email }} <br>
                            <strong>Technikos pavadinimas, modelis:</strong> {{ $order->device }} <br>
                            <strong>Serijinis numeris:</strong> {{ $order->serial }} <br>
                            <strong>Komplektacija:</strong> {{ $order->package }} <br>
                            <strong>Gedimo aprašymas:</strong> {{ $order->defect }} <br>
                            <strong>Prioritetas:</strong> {{ $order->priority }} <br>
                            <strong>Pastabos:</strong> {{ $order->notes }}
                        </td>

                    </tr>

                    </tbody>

                </table>

                <p class="text-justify">{!! nl2br(e($order->contract->contract)) !!}</p>

                <table class="table">

                    <tbody>

                    <tr>

                        <td class="col-xs-6">Prekę priėmiau: {{ $order->employee }}</td>
                        <td class="col-xs-6">Su taisymo sąlygomis susipažinau ir sutinku:</td>

                    </tr>

                    </tbody>

                </table>

                <div class="form-group">
                    <div class="col-xs-6 col-xs-offset-4">
                        <button type="submit" class="btn btn-primary print">
                            Spausdinti
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-sm-12 visible-print-block">


        <p class="panel-heading text-center">
            Kompiuterinės technikos testavimo / taisymo sutartis
            Nr. {{ str_pad($order->id, 5, '0', STR_PAD_LEFT) }}
            <br>
            Priėmimo data: {{ date('Y-m-d', strtotime($order->created_at)) }}
        </p>

        <table class="table">

            <thead>
            <tr>
                <th class="col-xs-6">Vykdytojas</th>
                <th class="col-xs-6">Užsakovas</th>
            </tr>
            </thead>

            <tbody>

            <tr>

                <td>
                    {{ $order->details->provider }} <br>
                    <strong>Adresas:</strong> {{ $order->details->address }} <br>
                    <strong>Telefonas:</strong> {{ $order->details->phone }} <br>
                    <strong>El. paštas:</strong> {{ $order->details->email }} <br>
                    <strong>Darbo laikas:</strong> {{ $order->details->hours }}
                </td>

                <td>
                    {{ $order->customer->customer }} <br>
                    <strong>Telefonas:</strong> {{ $order->customer->phone }} <br>
                    <strong>El. paštas:</strong> {{ $order->customer->email }} <br>
                    <strong>Technikos pavadinimas, modelis:</strong> {{ $order->device }} <br>
                    <strong>Serijinis numeris:</strong> {{ $order->serial }} <br>
                    <strong>Komplektacija:</strong> {{ $order->package }} <br>
                    <strong>Gedimo aprašymas:</strong> {{ $order->defect }} <br>
                    <strong>Prioritetas:</strong> {{ $order->priority }} <br>
                    <strong>Pastabos:</strong> {{ $order->notes }}
                </td>

            </tr>

            </tbody>

        </table>

        <p class="text-justify">{!! nl2br(e($order->contract->contract)) !!}</p>

        <table class="table">

            <tbody>

            <tr>

                <td class="col-xs-6">Prekę priėmiau: {{ $order->employee }}</td>
                <td class="col-xs-6">Su taisymo sąlygomis susipažinau ir sutinku:</td>

            </tr>

            </tbody>

        </table>


    </div>


@endsection
