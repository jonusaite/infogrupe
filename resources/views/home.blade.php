@extends('layouts.app')

@section('content')

    <div class="col-sm-9">
        <div class="panel panel-default">
            <div class="panel-heading">Sveiki!</div>

            <div class="panel-body">
                @if (Auth::guest())
                    Norėdami naudotis sistema, prisijunkite.
                @else
                    Jūs esate prisijungę. Navigacijoje pasirinkite norimus atlikti veiksmus.
                @endif
            </div>
        </div>
    </div>


@endsection
