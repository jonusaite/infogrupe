@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">Pakeisti el. pašto adresą</div>

            <div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('changeEmail') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">El. paštas</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email"
                                   value="{{ Auth::user()->email }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Pakeisti
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

    <div class="col-sm-9">
        <div class="panel panel-default">
            <div class="panel-heading">Pakeisti slaptažodį</div>

            <div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('changePassword') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('currentPassword') ? ' has-error' : '' }}">
                        <label for="currentPassword" class="col-md-4 control-label">Dabartinis slaptažodis</label>

                        <div class="col-md-6">
                            <input id="currentPassword" type="password" class="form-control" name="currentPassword"
                                   required>

                            @if ($errors->has('currentPassword'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('currentPassword') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('newPassword') ? ' has-error' : '' }}">
                        <label for="newPassword" class="col-md-4 control-label">Naujas slaptažodis</label>

                        <div class="col-md-6">
                            <input id="newPassword" type="password" class="form-control" name="newPassword" required>

                            @if ($errors->has('newPassword'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('newPassword') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('newPassword_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="col-md-4 control-label">Pakartoti naują slaptažodį</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control"
                                   name="newPassword_confirmation" required>

                            @if ($errors->has('newPassword_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('newPassword_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Pakeisti
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
