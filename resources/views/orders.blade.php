@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        <div class="panel panel-default">
            <div class="panel-heading">Užsakymų sąrašas</div>

            <div class="panel-body table-responsive">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('searchOrder') }}">
                    {{ csrf_field() }}

                    <table class="table table-hover">

                        <thead>
                        <tr>
                            <th class="col-xs-1"><label for="id" class="control-label">Nr.</label></th>
                            <th class="col-xs-1">Data</th>
                            <th class="col-xs-3"><label for="customer" class="control-label">Užsakovas</label></th>
                            <th class="col-xs-4"><label for="device" class="control-label">Technikos pavadinimas</label>
                            </th>
                            <th class="col-xs-2">Veiksmai</th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>
                            <td><input id="id" type="text" class="form-control" name="id"></td>
                            <td></td>
                            <td>
                                <select id="customer" class="form-control" name="customer">
                                    <option value="">Visi</option>
                                    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->customer }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td><input id="device" type="text" class="form-control" name="device"></td>
                            <td>
                                <button type="submit" class="btn btn-primary">
                                    Ieškoti
                                </button>
                            </td>
                        </tr>

                        @foreach ($orders as $order)
                            <tr data-order-id="{{ $order->id }}">
                                <td>{{ str_pad($order->id, 5, '0', STR_PAD_LEFT) }}</td>
                                <td>{{ date('Y-m-d', strtotime($order->created_at)) }}</td>
                                <td>{{ $order->customer->customer }}</td>
                                <td>{{ $order->device }}</td>
                                <td>
                                    <a class="btn btn-primary" title="Spausdinti"
                                       href="{{ route('print', $order->id) }}"><i
                                                class="glyphicon glyphicon-print" aria-hidden="true"></i></a>
                                    <a class="btn btn-success" title="Redaguoti"
                                       href="{{ route('editOrder', $order->id) }}"><i
                                                class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                    <button type="button" class="btn btn-danger deleteOrder" title="Ištrinti"
                                            value="{{ $order->id }}">
                                        <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>

                </form>

            </div>
        </div>
    </div>

@endsection
