@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        <div class="panel panel-default">
            <div class="panel-heading">Redaguoti užsakymą nr. {{ $order->id }}</div>

            <div class="panel-body table-responsive">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('saveOrder', $order->id) }}">
                    {{ csrf_field() }}

                    <table class="table">

                        <thead>
                        <tr>
                            <th class="col-xs-3">Vykdytojas</th>
                            <th class="col-xs-4">Užsakovas</th>
                            <th class="col-xs-5"></th>
                        </tr>
                        </thead>

                        <tbody>

                        <tr>

                            <td rowspan="9">
                                <strong>Vykdytojas:</strong>
                                <div id="refreshProvider">{{ $order->details->provider }}</div>
                                <strong>Adresas:</strong>
                                <div id="refreshAddress">{{ $order->details->address }}</div>
                                <strong>Telefonas:</strong>
                                <div id="refreshPhone">{{ $order->details->phone }}</div>
                                <strong>El. paštas:</strong>
                                <div id="refreshEmail">{{ $order->details->email }}</div>
                                <strong>Darbo laikas:</strong>
                                <div id="refreshHours">{{ $order->details->hours }}</div>
                                <button type="button" class="btn btn-primary refreshDetails" title="Atnaujinti">
                                    Atnaujinti rekvizitus
                                </button>
                            </td>

                            <td><label for="customerInfo" class="control-label">Užsakovas</label></td>

                            <td>
                                <select id="customerInfo" class="form-control" name="customer_id" required autofocus>
                                    <option value="">-- Pasirinkite užsakovą</option>
                                    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}" {{ ($customer->id === $order->customer->id) ? 'selected' : '' }}>{{ $customer->customer }}</option>
                                    @endforeach
                                </select>
                            </td>

                        </tr>

                        <tr>
                            <td><strong>Telefonas</strong></td>
                            <td id="phoneInfo">{{ $order->customer->phone }}</td>
                        </tr>

                        <tr>
                            <td><strong>El. paštas</strong></td>
                            <td id="emailInfo">{{ $order->customer->email }}</td>
                        </tr>

                        <tr>
                            <td><label for="device" class="control-label">Technikos pavadinimas, modelis</label></td>
                            <td><input id="device" type="text" class="form-control" name="device"
                                       value="{{ old('device') ? old('device') : $order->device }}" required autofocus>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="serial" class="control-label">Serijinis numeris</label></td>
                            <td><input id="serial" type="text" class="form-control" name="serial"
                                       value="{{ old('serial') ? old('serial') : $order->serial }}" required autofocus>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="package" class="control-label">Komplektacija</label></td>
                            <td><input id="package" type="text" class="form-control" name="package"
                                       value="{{ old('package') ? old('package') : $order->package }}" required
                                       autofocus></td>
                        </tr>

                        <tr>
                            <td><label for="defect" class="control-label">Gedimo aprašymas</label></td>
                            <td><input id="defect" type="text" class="form-control" name="defect"
                                       value="{{ old('defect') ? old('defect') : $order->defect }}" required autofocus>
                            </td>
                        </tr>

                        <tr>
                            <td><label for="priority" class="control-label">Prioritetas</label></td>
                            <td><input id="priority" type="text" class="form-control" name="priority"
                                       value="{{ old('priority') ? old('priority') : $order->priority }}" required
                                       autofocus></td>
                        </tr>

                        <tr>
                            <td><label for="notes" class="control-label">Pastabos</label></td>
                            <td><textarea id="notes" class="form-control fixed-width" name="notes"
                                          autofocus>{{ old('notes') ? old('notes') : $order->notes }}</textarea></td>
                        </tr>
                        </tbody>
                    </table>

                    <input id="refreshDetailsId" type="hidden" name="details_id" value="{{ $order->details->id }}">
                    <input id="refreshContractId" type="hidden" name="contract_id" value="{{ $order->contract->id }}">
                    <textarea id="refreshContract" class="form-control fixed-width" rows="10" readonly>{{ $order->contract->contract }}</textarea>
                    <button type="button" class="btn btn-primary refreshContract" title="Atnaujinti"
                            value="{{ $order->id }}">
                        Atnaujinti sutartį
                    </button>
                    <table>
                        <tbody>
                        <tr>
                            <td class="col-xs-3"><label for="employee" class="control-label">Prekę priėmiau:</label>
                            </td>
                            <td class="col-xs-4"><input id="employee" type="text" class="form-control" name="employee"
                                                        value="{{ old('employee') ? old('employee') : $order->employee }}"
                                                        required autofocus></td>
                            <td class="col-xs-5">Su taisymo sąlygomis susipažinau ir sutinku:</td>
                        </tr>

                        </tbody>

                    </table>
                    <br>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Išsaugoti
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
