@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">Užsakovų sąrašas
                <a class="btn btn-primary col-sm-offset-6" href="{{ route('newCustomer') }}">Pridėti užsakovą</a>
            </div>

            <div class="panel-body table-responsive">

                <table class="table table-hover">

                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Užsakovas</th>
                        <th>Telefonas</th>
                        <th>El. paštas</th>
                        <th>Veiksmai</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($customers as $customer)
                        <tr data-client-id="{{ $customer->id }}">
                            <td>{{ $customer->id }}</td>
                            <td>{{ $customer->customer }}</td>
                            <td>{{ $customer->phone }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>
                                <a class="btn btn-success" title="Redaguoti"
                                   href="{{ route('editCustomer', $customer->id) }}"><i
                                            class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                @if ($customer->order->isEmpty())
                                <button type="submit" class="btn btn-danger deleteCustomer" title="Ištrinti" value="{{ $customer->id }}">
                                    <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                                </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

            </div>
        </div>
    </div>

@endsection
