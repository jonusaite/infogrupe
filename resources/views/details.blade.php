@extends('layouts.app')

@section('content')

    <div class="col-sm-9">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">Redaguoti rekvizitus</div>

            <div class="panel-body">

                <form class="form-horizontal" role="form" method="POST" action="{{ route('editDetails') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('provider') ? ' has-error' : '' }}">
                        <label for="provider" class="col-md-4 control-label">Vykdytojas</label>

                        <div class="col-md-6">
                            <input id="provider" type="text" class="form-control" name="provider"
                                   value="{{ old('provider') ? old('provider') : (($details) ? $details->provider : '') }}" required autofocus>

                            @if ($errors->has('provider'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('provider') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                        <label for="address" class="col-md-4 control-label">Adresas</label>

                        <div class="col-md-6">
                            <input id="address" type="text" class="form-control" name="address"
                                   value="{{ old('address') ? old('address') : (($details) ? $details->address : '') }}" required autofocus>

                            @if ($errors->has('address'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone" class="col-md-4 control-label">Telefonas</label>

                        <div class="col-md-6">
                            <input id="phone" type="text" class="form-control" name="phone"
                                   value="{{ old('phone') ? old('phone') : (($details) ? $details->phone : '') }}" required autofocus>

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">El. pašto adresas</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') ? old('email') : (($details) ? $details->email : '') }}"
                                   required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('hours') ? ' has-error' : '' }}">
                        <label for="hours" class="col-md-4 control-label">Darbo laikas</label>

                        <div class="col-md-6">
                            <input id="hours" type="text" class="form-control" name="hours"
                                   value="{{ old('hours') ? old('hours') : (($details) ? $details->hours : '') }}" required autofocus>

                            @if ($errors->has('hours'))
                                <span class="help-block">
                                         <strong>{{ $errors->first('hours') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Išsaugoti
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection
