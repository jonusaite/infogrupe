<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Slaptažodis turi būti bent 6 simboliai ir atitikti pakartotą slaptažodį.',
    'reset' => 'Jūsų slaptažodis buvo atstatytas!',
    'sent' => 'Slaptažodžio atstatymo nuoroda išsiųsta Jūsų el. paštu!',
    'token' => 'Ši slaptažodžio atstatymuo nuoroda negalioja.',
    'user' => "Vartotojo su tokiu el. pašto adresu nėra.",

];
