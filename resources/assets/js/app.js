/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});


/**
 * Add customer phone and email from selection.
 */
$('#customerInfo').change(function () {
    if ($('option:selected', this).val() !== '') {
        $.get(
            'customerInfo/' + $('option:selected', this).val(),
            function (data) {
                $('#phoneInfo').text(data.phone);
                $('#emailInfo').text(data.email);
            }, 'json')
    } else {
        $('#phoneInfo').text('');
        $('#emailInfo').text('');
    }
});

/**
 * Refresh contract in order.
 */
$('.refreshContract').on('click', function() {
    $.get(
        '../refreshContract',
        function (data) {
            $('#refreshContract').text(data.contract);
            $('#refreshContractId').val(data.id);
        }, 'json')
});

/**
 * Refresh details in order.
 */
$('.refreshDetails').on('click', function() {
    $.get(
        '../refreshDetails',
        function (data) {
            $('#refreshProvider').text(data.details['provider']);
            $('#refreshAddress').text(data.details['address']);
            $('#refreshPhone').text(data.details['phone']);
            $('#refreshEmail').text(data.details['email']);
            $('#refreshHours').text(data.details['hours']);
            $('#refreshDetailsId').val(data.details['id']);
        }, 'json')
});

/**
 * Asks for confirmation before deleting customer.
 */
$('.deleteCustomer').on('click', function () {
    if (confirm('Ar tikrai norite ištrinti šį užsakovą?')) {
        $.get(
            'deleteCustomer/' + $(this).attr('value')
        );
        $('tr[data-client-id="' + $(this).attr('value') + '"]').remove()
    }
});

/**
 * Asks for confirmation before deleting order.
 */
$('.deleteOrder').on('click', function () {
    if (confirm('Ar tikrai norite ištrinti šį užsakymą?')) {
        $.get(
            'deleteOrder/' + $(this).attr('value')
        );
        $('tr[data-order-id="' + $(this).attr('value') + '"]').remove()
    }
});

/**
 * Print order contract.
 */
$('.print').on('click', function () {
    window.print();
});