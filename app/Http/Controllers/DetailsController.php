<?php

namespace App\Http\Controllers;

use App\Details;
use App\Http\Requests\DetailsRequest;
use Illuminate\Http\Request;

class DetailsController extends Controller
{
    /**
     * View and edit details.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details()
    {
        return view('details', [
            'details' => Details::latest()->first()
        ]);
    }

    /**
     * Edit details (inserts new entry to database)
     *
     * @param DetailsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editDetails(DetailsRequest $request)
    {
        $details = new Details();
        $details->provider = $request->provider;
        $details->address = $request->address;
        $details->phone = $request->phone;
        $details->email = $request->email;
        $details->hours = $request->hours;
        $details->save();
        return redirect()->back()->with('status', 'Rekvizitai sėkmingai pakeisti!');
    }

    /**
     * Refresh details in order.
     *
     * @return string
     */
    public function refreshDetails()
    {
        $details = Details::latest()->first();
        $data = array(
            'details' => $details
        );
        return json_encode($data);
    }
}
