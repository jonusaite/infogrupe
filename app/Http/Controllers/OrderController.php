<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Customer;
use App\Details;
use App\Http\Requests\OrderRequest;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * View new order form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order()
    {
        return view('order', [
            'contract' => Contract::latest()->first(),
            'details' => Details::latest()->first(),
            'customers' => Customer::all()
        ]);
    }

    /**
     * Save new order and save employee cookie.
     *
     * @param OrderRequest $request
     * @return $this
     */
    public function newOrder(OrderRequest $request)
    {
        $order = new Order();
        $order->customer_id = $request->customer_id;
        $order->device = $request->device;
        $order->serial = $request->serial;
        $order->package = $request->package;
        $order->defect = $request->defect;
        $order->priority = $request->priority;
        $order->notes = $request->notes;
        $order->employee = $request->employee;
        $cookie = cookie('employee', $request->employee);
        $order->details_id = $request->details_id;
        $order->contract_id = $request->contract_id;
        $order->save();
        return redirect()->route('print', $order->id)->cookie($cookie);
    }

    /**
     * View order before printing.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function printPreview($id)
    {
        return view('print', [
            'order' => Order::find($id)
        ]);
    }

    /**
     * Display all orders.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orders()
    {
        return view('orders', [
            'orders' => Order::all(),
            'customers' => Customer::all()
        ]);
    }

    /**
     * Delete order.
     *
     * @param $id
     */
    public function deleteOrder($id)
    {
        $order = Order::find($id);
        $order->delete();
    }

    /**
     * View edit order page.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editOrder($id)
    {
        return view('editOrder', [
            'order' => Order::find($id),
            'customers' => Customer::all()
        ]);
    }

    /**
     * Save edited order.
     *
     * @param $id
     * @param OrderRequest $request
     * @return $this
     */
    public function saveOrder($id, OrderRequest $request)
    {
        $order = Order::find($id);
        $order->customer_id = $request->customer_id;
        $order->device = $request->device;
        $order->serial = $request->serial;
        $order->package = $request->package;
        $order->defect = $request->defect;
        $order->priority = $request->priority;
        $order->notes = $request->notes;
        $order->employee = $request->employee;
        $cookie = cookie('employee', $request->employee);
        $order->details_id = $request->details_id;
        $order->contract_id = $request->contract_id;
        $order->save();
        return redirect()->route('print', $order->id)->cookie($cookie);
    }

    /**
     * Search by id, device or customer.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchOrder(Request $request)
    {
        $result = Order::where(function ($query) use ($request) {
            if ($request->id != '') $query->where('id', '=', (int)$request->id);
            if ($request->device != '') $query->where('device', 'like', "%$request->device%");
            if ($request->customer != '') $query->whereHas('customer', function ($query) use ($request) {
                $query->where('id', '=', $request->customer);
            });
        })->get();
        return view('orders', [
            'orders' => $result,
            'customers' => Customer::all()
        ]);
    }
}
