<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * View customers list.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewCustomers()
    {
        return view('customers', [
            'customers' => Customer::all()
        ]);
    }

    /**
     * View add new customer page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newCustomer()
    {
        return view('newCustomer');
    }

    /**
     * Add new customer.
     *
     * @param CustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addCustomer(CustomerRequest $request)
    {
        $customer = new Customer();
        $customer->customer = $request->customer;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->save();
        return redirect()->back()->with('status', "Užsakovas $request->customer sėkmingai pridėtas!");
    }

    /**
     * Delete customer.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteCustomer($id)
    {
        $customer = Customer::find($id);
        if ($customer->order->isEmpty()) {
            $customer->delete();
        }
    }

    /**
     * View edit customer page.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editCustomer($id)
    {
        return view('editCustomer', [
            'customer' => Customer::find($id)
        ]);
    }

    /**
     * Save edited customer.
     *
     * @param $id
     * @param CustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveCustomer($id, CustomerRequest $request)
    {
        $customer = Customer::find($id);
        $customer->customer = $request->customer;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->save();
        return redirect()->back()->with('status', "Užsakovo $request->customer duomenys sėkmingai pakeisti!");
    }

    /**
     * Get customer phone and email and show in the order.
     *
     * @param $id
     * @return string
     */
    public function customerInfo($id)
    {
        $customer = Customer::find($id);
        $info = array(
            'phone' => $customer->phone,
            'email' => $customer->email
        );
        return json_encode($info);
    }
}
