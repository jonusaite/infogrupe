<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Http\Requests\ContractRequest;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    /**
     * View and edit contract.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contract()
    {
        return view('contract', [
                'contract' => Contract::latest()->first()
            ]);
    }

    /**
     * Edit contract (inserts new entry to database).
     *
     * @param ContractRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editContract(ContractRequest $request) {
        $contract = new Contract();
        $contract->contract = $request->contract;
        $contract->save();
        return redirect()->back()->with('status', 'Sutartis sėkmingai pakeista!');
    }

    /**
     * Refresh contract in order.
     *
     * @param $id
     * @return string
     */
    public function refreshContract() {
        $contract = Contract::latest()->first();
        $data = array(
            'id' => $contract->id,
            'contract' => $contract->contract
        );
        return json_encode($data);
    }
}
