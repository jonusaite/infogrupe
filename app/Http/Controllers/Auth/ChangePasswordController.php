<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\PasswordRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    /**
     * Change password.
     *
     * @param PasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(PasswordRequest $request)
    {
        $user = Auth::user();
        if (Hash::check($request->currentPassword, $user->password)) {
            $user->password = Hash::make($request->newPassword);
            $user->save();
            return redirect()->back()->with('status', 'Slaptažodis sėkmingai pakeistas');
        } else {
            return redirect()->back()->with('error', 'Slaptažodis neteisingas');
        }
    }
}
