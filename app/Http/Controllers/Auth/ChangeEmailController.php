<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\EmailRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChangeEmailController extends Controller
{
    /**
     * Change e-mail.
     *
     * @param EmailRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeEmail(EmailRequest $request)
    {
        $user = Auth::user();
        $user->email = $request->email;
        $user->save();
        return redirect()->back()->with('status', 'El. paštas sėkmingai pakeistas');
    }
}
