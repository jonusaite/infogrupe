<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:191|unique:users'
        ];
    }

    public function messages()
    {
        return [
            'email.max' => 'El. pašto adresas turi būti ne ilgesnis negu :max simboliai.',
            'email.unique' => 'Toks el. pašto adresas jau egzistuoja.'
        ];
    }
}
