<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => 'required|max:191',
            'address' => 'required|max:191',
            'phone' => array(
                'required',
                'string',
                'min:8',
                'max:20',
                'regex:/^(\+|8|00)[0-9]+/'
            ),
            'email' => 'required|string|email|max:191',
            'hours' => 'required|max:191'
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => 'Telefonas turi prasidėti „+“, „8“ ara „00“ ir būti sudarytas tik iš skaičių.'
        ];
    }
}
