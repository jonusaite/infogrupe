## Contracts registration system for computer repairment. 

System for computer repairment contracts registration. Registration, adding company details, contract description, new clients, printint contract, editing and deleting. Refreshing company details and contract description in old contracts with ajax. Changing e-mail and password, reminding e-mail. 

1. Install and configure Laravel. Instructions: [https://laravel.com/docs/5.4](https://laravel.com/docs/5.4)

2. Copy over files from repo.

3. Migrate databases. Instructions: [https://laravel.com/docs/5.4/migrations](https://laravel.com/docs/5.4/migrations)

Copyright (c) 2017 Aistė Jonušaitė

Licensed under [MIT License](https://opensource.org/licenses/mit-license.html)