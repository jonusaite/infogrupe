-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2017 at 05:18 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `infogrupe`
--

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `contract` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`id`, `contract`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales tristique dolor. Sed consequat orci a nunc sodales, ut ullamcorper elit dapibus. Phasellus nec pulvinar augue. Donec enim ante, blandit in semper sit amet, ornare ornare sem. Ut elementum odio ornare neque interdum imperdiet. Maecenas massa elit, interdum sed tortor pellentesque, auctor interdum ipsum. Maecenas viverra imperdiet risus eu malesuada. In nec lacinia turpis. Phasellus orci lectus, efficitur ac risus sollicitudin, porttitor ullamcorper nisi. Nunc ut arcu finibus, aliquam elit nec, congue nunc. Sed sed magna non ipsum interdum viverra id nec lacus.\r\n\r\nSed gravida sit amet sem sit amet egestas. Nullam quam nunc, sagittis sed orci eget, malesuada sagittis nulla. Aenean tincidunt sapien dui, ut maximus orci aliquet vitae. Nam eu blandit dui, nec tincidunt augue. Nam sit amet vestibulum diam. Sed dapibus est ac massa hendrerit iaculis. Ut rhoncus orci non eros lacinia egestas. Fusce a nibh erat. Fusce mauris ante, gravida sed est nec, faucibus mollis diam. Maecenas volutpat non ante id rhoncus. Nunc consectetur maximus metus. Fusce eget condimentum nibh, in elementum orci. Duis aliquam aliquam quam at commodo. Mauris eleifend ipsum vel diam malesuada, id tempor justo tincidunt. Vivamus augue felis, interdum sit amet aliquam a, tempus at leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\r\n\r\nVivamus leo ante, finibus at nunc in, mollis sodales leo. Proin ornare nunc eget hendrerit condimentum. Quisque posuere magna eu eleifend pretium. Maecenas efficitur lacinia nulla, nec eleifend nisi pulvinar at. Sed a lectus orci. Curabitur in magna in diam ultrices tristique venenatis quis risus. Phasellus eu lobortis nunc. Nam molestie, dui eu sagittis sodales, magna erat dapibus purus, nec tempus augue tortor vitae ex. Aliquam sit amet suscipit ipsum. Quisque pharetra ex id nisi feugiat volutpat. Vivamus consectetur lacinia velit, id ornare enim posuere id. Suspendisse lacus nibh, vulputate quis elit quis, facilisis viverra dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas in leo nec nisl tincidunt aliquet. Duis egestas lacinia massa, sit amet accumsan leo. Aliquam suscipit, felis ut rhoncus vestibulum, neque ligula malesuada ipsum, a convallis ipsum turpis eget velit.\r\n\r\nProin pulvinar mauris ac dolor accumsan, eu bibendum felis viverra. Ut dictum vitae metus et tristique. Vestibulum pulvinar sit amet dui quis molestie. Sed tristique dolor ac magna faucibus auctor. Mauris sit amet nisi non arcu sollicitudin vehicula. Donec aliquam bibendum luctus. In sapien tellus, varius ut nulla a, tempor elementum est. Fusce eu sollicitudin risus, eu blandit tortor. Proin eu dapibus justo. Nam orci ex, ultricies eu facilisis ut, scelerisque consectetur augue. Praesent luctus vel lectus quis congue. Mauris quis mauris eu urna feugiat pretium.\r\n\r\nPellentesque id tempor nunc. Ut pretium dictum massa, a euismod mi venenatis ut. Proin eu accumsan urna, non ultrices massa. Integer non pellentesque libero, quis fermentum felis. Duis in eleifend sem, id pharetra est. Cras vel auctor sem. Praesent pretium nibh sit amet luctus tincidunt. Vivamus condimentum justo sed pellentesque mollis. Etiam a laoreet tellus. Maecenas pellentesque lacus sit amet justo laoreet, eget tincidunt elit consequat.', '2017-06-27 10:03:06', '2017-06-27 10:03:06'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales tristique dolor. Sed consequat orci a nunc sodales, ut ullamcorper elit dapibus. Phasellus nec pulvinar augue. Donec enim ante, blandit in semper sit amet, ornare ornare sem. Ut elementum odio ornare neque interdum imperdiet. Maecenas massa elit, interdum sed tortor pellentesque, auctor interdum ipsum. Maecenas viverra imperdiet risus eu malesuada. In nec lacinia turpis. Phasellus orci lectus, efficitur ac risus sollicitudin, porttitor ullamcorper nisi. Nunc ut arcu finibus, aliquam elit nec, congue nunc. Sed sed magna non ipsum interdum viverra id nec lacus.\r\n\r\nSed gravida sit amet sem sit amet egestas. Nullam quam nunc, sagittis sed orci eget, malesuada sagittis nulla. Aenean tincidunt sapien dui, ut maximus orci aliquet vitae. Nam eu blandit dui, nec tincidunt augue. Nam sit amet vestibulum diam. Sed dapibus est ac massa hendrerit iaculis. Ut rhoncus orci non eros lacinia egestas. Fusce a nibh erat. Fusce mauris ante, gravida sed est nec, faucibus mollis diam. Maecenas volutpat non ante id rhoncus. Nunc consectetur maximus metus. Fusce eget condimentum nibh, in elementum orci. Duis aliquam aliquam quam at commodo. Mauris eleifend ipsum vel diam malesuada, id tempor justo tincidunt. Vivamus augue felis, interdum sit amet aliquam a, tempus at leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\r\n\r\nVivamus leo ante, finibus at nunc in, mollis sodales leo. Proin ornare nunc eget hendrerit condimentum. Quisque posuere magna eu eleifend pretium. Maecenas efficitur lacinia nulla, nec eleifend nisi pulvinar at. Sed a lectus orci. Curabitur in magna in diam ultrices tristique venenatis quis risus. Phasellus eu lobortis nunc. Nam molestie, dui eu sagittis sodales, magna erat dapibus purus, nec tempus augue tortor vitae ex. Aliquam sit amet suscipit ipsum. Quisque pharetra ex id nisi feugiat volutpat. Vivamus consectetur lacinia velit, id ornare enim posuere id. Suspendisse lacus nibh, vulputate quis elit quis, facilisis viverra dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas in leo nec nisl tincidunt aliquet. Duis egestas lacinia massa, sit amet accumsan leo. Aliquam suscipit, felis ut rhoncus vestibulum, neque ligula malesuada ipsum, a convallis ipsum turpis eget velit.', '2017-06-27 10:04:18', '2017-06-27 10:04:18'),
(3, 'Donec sodales tristique dolor. Sed consequat orci a nunc sodales, ut ullamcorper elit dapibus. Phasellus nec pulvinar augue. Donec enim ante, blandit in semper sit amet, ornare ornare sem. Ut elementum odio ornare neque interdum imperdiet. Maecenas massa elit, interdum sed tortor pellentesque, auctor interdum ipsum. Maecenas viverra imperdiet risus eu malesuada. In nec lacinia turpis. Phasellus orci lectus, efficitur ac risus sollicitudin, porttitor ullamcorper nisi. Nunc ut arcu finibus, aliquam elit nec, congue nunc. Sed sed magna non ipsum interdum viverra id nec lacus.\r\n\r\nSed gravida sit amet sem sit amet egestas. Nullam quam nunc, sagittis sed orci eget, malesuada sagittis nulla. Aenean tincidunt sapien dui, ut maximus orci aliquet vitae. Nam eu blandit dui, nec tincidunt augue. Nam sit amet vestibulum diam. Sed dapibus est ac massa hendrerit iaculis. Ut rhoncus orci non eros lacinia egestas. Fusce a nibh erat. Fusce mauris ante, gravida sed est nec, faucibus mollis diam. Maecenas volutpat non ante id rhoncus. Nunc consectetur maximus metus. Fusce eget condimentum nibh, in elementum orci. Duis aliquam aliquam quam at commodo. Mauris eleifend ipsum vel diam malesuada, id tempor justo tincidunt. Vivamus augue felis, interdum sit amet aliquam a, tempus at leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\r\n\r\nVivamus leo ante, finibus at nunc in, mollis sodales leo. Proin ornare nunc eget hendrerit condimentum. Quisque posuere magna eu eleifend pretium. Maecenas efficitur lacinia nulla, nec eleifend nisi pulvinar at. Sed a lectus orci. Curabitur in magna in diam ultrices tristique venenatis quis risus. Phasellus eu lobortis nunc. Nam molestie, dui eu sagittis sodales, magna erat dapibus purus, nec tempus augue tortor vitae ex. Aliquam sit amet suscipit ipsum. Quisque pharetra ex id nisi feugiat volutpat. Vivamus consectetur lacinia velit, id ornare enim posuere id. Suspendisse lacus nibh, vulputate quis elit quis, facilisis viverra dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas in leo nec nisl tincidunt aliquet. Duis egestas lacinia massa, sit amet accumsan leo. Aliquam suscipit, felis ut rhoncus vestibulum, neque ligula malesuada ipsum, a convallis ipsum turpis eget velit.', '2017-06-27 10:25:34', '2017-06-27 10:25:34'),
(4, 'Donec sodales tristique dolor. Sed consequat orci a nunc sodales, ut ullamcorper elit dapibus. Phasellus nec pulvinar augue. Donec enim ante, blandit in semper sit amet, ornare ornare sem. Ut elementum odio ornare neque interdum imperdiet. Maecenas massa elit, interdum sed tortor pellentesque, auctor interdum ipsum. Maecenas viverra imperdiet risus eu malesuada. In nec lacinia turpis. Phasellus orci lectus, efficitur ac risus sollicitudin, porttitor ullamcorper nisi. Nunc ut arcu finibus, aliquam elit nec, congue nunc. Sed sed magna non ipsum interdum viverra id nec lacus.\r\n\r\nSed gravida sit amet sem sit amet egestas. Nullam quam nunc, sagittis sed orci eget, malesuada sagittis nulla. Aenean tincidunt sapien dui, ut maximus orci aliquet vitae. Nam eu blandit dui, nec tincidunt augue. Nam sit amet vestibulum diam. Sed dapibus est ac massa hendrerit iaculis. Ut rhoncus orci non eros lacinia egestas. Fusce a nibh erat. Fusce mauris ante, gravida sed est nec, faucibus mollis diam. Maecenas volutpat non ante id rhoncus. Nunc consectetur maximus metus. Fusce eget condimentum nibh, in elementum orci. Duis aliquam aliquam quam at commodo. Mauris eleifend ipsum vel diam malesuada, id tempor justo tincidunt. Vivamus augue felis, interdum sit amet aliquam a, tempus at leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.\r\n\r\nVivamus leo ante, finibus at nunc in, mollis sodales leo. Proin ornare nunc eget hendrerit condimentum. Quisque posuere magna eu eleifend pretium. Maecenas efficitur lacinia nulla, nec eleifend nisi pulvinar at. Sed a lectus orci. Curabitur in magna in diam ultrices tristique venenatis quis risus. Phasellus eu lobortis nunc.', '2017-06-27 10:29:29', '2017-06-27 10:29:29'),
(5, 'Donec sodales tristique dolor. Sed consequat orci a nunc sodales, ut ullamcorper elit dapibus. Phasellus nec pulvinar augue. Donec enim ante, blandit in semper sit amet, ornare ornare sem. Ut elementum odio ornare neque interdum imperdiet. Maecenas massa elit, interdum sed tortor pellentesque, auctor interdum ipsum. Maecenas viverra imperdiet risus eu malesuada. In nec lacinia turpis. Phasellus orci lectus, efficitur ac risus sollicitudin, porttitor ullamcorper nisi. Nunc ut arcu finibus, aliquam elit nec, congue nunc. Sed sed magna non ipsum interdum viverra id nec lacus.\r\n\r\nSed gravida sit amet sem sit amet egestas. Nullam quam nunc, sagittis sed orci eget, malesuada sagittis nulla. Aenean tincidunt sapien dui, ut maximus orci aliquet vitae. Nam eu blandit dui, nec tincidunt augue. Nam sit amet vestibulum diam. Sed dapibus est ac massa hendrerit iaculis. Ut rhoncus orci non eros lacinia egestas. Fusce a nibh erat. Fusce mauris ante, gravida sed est nec, faucibus mollis diam. Maecenas volutpat non ante id rhoncus. Nunc consectetur maximus metus. Fusce eget condimentum nibh, in elementum orci. Duis aliquam aliquam quam at commodo. Mauris eleifend ipsum vel diam malesuada, id tempor justo tincidunt. Vivamus augue felis, interdum sit amet aliquam a, tempus at leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-06-27 10:30:01', '2017-06-27 10:30:01'),
(6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.\r\n\r\nMaecenas vel lacinia lorem. Morbi vel est aliquet, ornare massa in, accumsan diam. Cras non tristique metus, vitae ullamcorper elit. Nam pretium magna ipsum, sed consequat turpis auctor eget. Nulla vitae enim dictum, euismod nibh vitae, tincidunt ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mi lorem, tincidunt eu efficitur eget, malesuada in risus. Etiam non sapien libero.\r\n\r\nCras ultricies dictum lorem sit amet porttitor. Curabitur efficitur urna ac interdum scelerisque. Vivamus viverra nisl eget felis bibendum, id fringilla libero tempor. Aenean lacus magna, vestibulum eu tempor eget, bibendum sed arcu. Sed mattis nisi ut lectus convallis, vel tempus lectus auctor. Morbi eget dictum elit. Etiam at vestibulum lacus, id suscipit nisl. Vestibulum id tortor ut augue accumsan congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nNullam auctor odio sed mollis accumsan. Nam est enim, pretium vitae tempus a, ultrices a libero. Mauris eu venenatis odio, imperdiet tincidunt tortor. Sed metus augue, feugiat in metus ac, eleifend porttitor est. Donec dapibus interdum odio at malesuada. Sed rhoncus tortor vitae erat rutrum, eget porttitor augue facilisis. In hac habitasse platea dictumst. In vitae interdum odio. Fusce enim dui, fringilla id purus vitae, lacinia dignissim mauris. Praesent tincidunt quis mi sed mattis.\r\n\r\nDonec pretium posuere tortor ut scelerisque. Nunc ac ex turpis. Aliquam neque risus, scelerisque vitae tellus vel, commodo tempus enim. Vivamus id risus quam. Duis mattis sapien quis semper maximus. Nullam luctus vehicula dolor, porta volutpat sem pellentesque auctor. Donec facilisis turpis elit, eget cursus tellus fermentum ultrices. Donec in vehicula elit. Aenean cursus, libero non malesuada commodo, augue mauris lacinia neque, quis suscipit lectus nulla sed urna. Etiam aliquet leo nec velit elementum, vel iaculis nisi lacinia. Nam ut pharetra libero, lobortis viverra odio. Mauris id pretium mi, vel fringilla justo. Quisque et sem consequat, fermentum arcu vitae, accumsan elit.', '2017-06-27 10:40:01', '2017-06-27 10:40:01'),
(7, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.\r\n\r\nMaecenas vel lacinia lorem. Morbi vel est aliquet, ornare massa in, accumsan diam. Cras non tristique metus, vitae ullamcorper elit. Nam pretium magna ipsum, sed consequat turpis auctor eget. Nulla vitae enim dictum, euismod nibh vitae, tincidunt ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mi lorem, tincidunt eu efficitur eget, malesuada in risus. Etiam non sapien libero.\r\n\r\nCras ultricies dictum lorem sit amet porttitor. Curabitur efficitur urna ac interdum scelerisque. Vivamus viverra nisl eget felis bibendum, id fringilla libero tempor. Aenean lacus magna, vestibulum eu tempor eget, bibendum sed arcu. Sed mattis nisi ut lectus convallis, vel tempus lectus auctor. Morbi eget dictum elit. Etiam at vestibulum lacus, id suscipit nisl. Vestibulum id tortor ut augue accumsan congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nNullam auctor odio sed mollis accumsan. Nam est enim, pretium vitae tempus a, ultrices a libero. Mauris eu venenatis odio, imperdiet tincidunt tortor. Sed metus augue, feugiat in metus ac, eleifend porttitor est. Donec dapibus interdum odio at malesuada. Sed rhoncus tortor vitae erat rutrum, eget porttitor augue facilisis. In hac habitasse platea dictumst. In vitae interdum odio. Fusce enim dui, fringilla id purus vitae, lacinia dignissim mauris. Praesent tincidunt quis mi sed mattis.', '2017-06-27 10:41:52', '2017-06-27 10:41:52'),
(8, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.\r\n\r\nMaecenas vel lacinia lorem. Morbi vel est aliquet, ornare massa in, accumsan diam. Cras non tristique metus, vitae ullamcorper elit. Nam pretium magna ipsum, sed consequat turpis auctor eget. Nulla vitae enim dictum, euismod nibh vitae, tincidunt ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mi lorem, tincidunt eu efficitur eget, malesuada in risus. Etiam non sapien libero.\r\n\r\nCras ultricies dictum lorem sit amet porttitor. Curabitur efficitur urna ac interdum scelerisque. Vivamus viverra nisl eget felis bibendum, id fringilla libero tempor. Aenean lacus magna, vestibulum eu tempor eget, bibendum sed arcu. Sed mattis nisi ut lectus convallis, vel tempus lectus auctor. Morbi eget dictum elit. Etiam at vestibulum lacus, id suscipit nisl. Vestibulum id tortor ut augue accumsan congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nNullam auctor odio sed mollis accumsan. Nam est enim, pretium vitae tempus a, ultrices a libero. Mauris eu venenatis odio, imperdiet tincidunt tortor. Sed metus augue, feugiat in metus ac, eleifend porttitor est. Donec dapibus interdum odio at malesuada. Sed rhoncus tortor vitae erat rutrum, eget porttitor augue facilisis. In hac habitasse platea dictumst. In vitae interdum odio. Fusce enim dui, fringilla id purus vitae, lacinia dignissim mauris. Praesent tincidunt quis mi sed mattis.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.', '2017-06-27 10:48:40', '2017-06-27 10:48:40'),
(9, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.\r\n\r\nMaecenas vel lacinia lorem. Morbi vel est aliquet, ornare massa in, accumsan diam. Cras non tristique metus, vitae ullamcorper elit. Nam pretium magna ipsum, sed consequat turpis auctor eget. Nulla vitae enim dictum, euismod nibh vitae, tincidunt ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mi lorem, tincidunt eu efficitur eget, malesuada in risus. Etiam non sapien libero.\r\n\r\nCras ultricies dictum lorem sit amet porttitor. Curabitur efficitur urna ac interdum scelerisque. Vivamus viverra nisl eget felis bibendum, id fringilla libero tempor. Aenean lacus magna, vestibulum eu tempor eget, bibendum sed arcu. Sed mattis nisi ut lectus convallis, vel tempus lectus auctor. Morbi eget dictum elit. Etiam at vestibulum lacus, id suscipit nisl. Vestibulum id tortor ut augue accumsan congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nNullam auctor odio sed mollis accumsan. Nam est enim, pretium vitae tempus a, ultrices a libero. Mauris eu venenatis odio, imperdiet tincidunt tortor. Sed metus augue, feugiat in metus ac, eleifend porttitor est. Donec dapibus interdum odio at malesuada. Sed rhoncus tortor vitae erat rutrum, eget porttitor augue facilisis. In hac habitasse platea dictumst. In vitae interdum odio. Fusce enim dui, fringilla id purus vitae, lacinia dignissim mauris. Praesent tincidunt quis mi sed mattis.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam.', '2017-06-27 11:22:57', '2017-06-27 11:22:57'),
(10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.\r\n\r\nMaecenas vel lacinia lorem. Morbi vel est aliquet, ornare massa in, accumsan diam. Cras non tristique metus, vitae ullamcorper elit. Nam pretium magna ipsum, sed consequat turpis auctor eget. Nulla vitae enim dictum, euismod nibh vitae, tincidunt ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mi lorem, tincidunt eu efficitur eget, malesuada in risus. Etiam non sapien libero.\r\n\r\nCras ultricies dictum lorem sit amet porttitor. Curabitur efficitur urna ac interdum scelerisque. Vivamus viverra nisl eget felis bibendum, id fringilla libero tempor. Aenean lacus magna, vestibulum eu tempor eget, bibendum sed arcu. Sed mattis nisi ut lectus convallis, vel tempus lectus auctor. Morbi eget dictum elit. Etiam at vestibulum lacus, id suscipit nisl. Vestibulum id tortor ut augue accumsan congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nNullam auctor odio sed mollis accumsan. Nam est enim, pretium vitae tempus a, ultrices a libero. Mauris eu venenatis odio, imperdiet tincidunt tortor. Sed metus augue, feugiat in metus ac, eleifend porttitor est. Donec dapibus interdum odio at malesuada. Sed rhoncus tortor vitae erat rutrum, eget porttitor augue facilisis. In hac habitasse platea dictumst. In vitae interdum odio. Fusce enim dui, fringilla id purus vitae, lacinia dignissim mauris. Praesent tincidunt quis mi sed mattis.', '2017-06-27 11:23:24', '2017-06-27 11:23:24'),
(11, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae metus sollicitudin mauris lacinia rutrum sit amet in augue. Proin eros nulla, aliquam sit amet eleifend vehicula, aliquet quis diam. Duis nec imperdiet ligula. Quisque quis erat dapibus, hendrerit mi eu, tristique mauris. Nunc vel dolor in dolor vulputate vehicula ut sit amet nisl. Phasellus lacinia lectus ut aliquam laoreet. Sed fermentum orci vitae finibus congue. Praesent luctus, ligula vitae varius tincidunt, nisl diam laoreet dolor, sit amet pharetra nunc ante ut odio. Pellentesque ultricies blandit sem ac egestas. Cras eget orci magna. Pellentesque consequat orci maximus purus auctor pulvinar. Cras non porttitor neque, ac tempus ligula.\r\n\r\nMaecenas vel lacinia lorem. Morbi vel est aliquet, ornare massa in, accumsan diam. Cras non tristique metus, vitae ullamcorper elit. Nam pretium magna ipsum, sed consequat turpis auctor eget. Nulla vitae enim dictum, euismod nibh vitae, tincidunt ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec mi lorem, tincidunt eu efficitur eget, malesuada in risus. Etiam non sapien libero.\r\n\r\nCras ultricies dictum lorem sit amet porttitor. Curabitur efficitur urna ac interdum scelerisque. Vivamus viverra nisl eget felis bibendum, id fringilla libero tempor. Aenean lacus magna, vestibulum eu tempor eget, bibendum sed arcu. Sed mattis nisi ut lectus convallis, vel tempus lectus auctor. Morbi eget dictum elit. Etiam at vestibulum lacus, id suscipit nisl. Vestibulum id tortor ut augue accumsan congue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2017-06-27 11:25:24', '2017-06-27 11:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customer`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'UAB \"XYZ\"', '+37062222333', 'xyz@example.com', '2017-06-27 10:01:50', '2017-06-27 10:01:50'),
(2, 'Vardas Pavardauskas', '+37068888888', 'pavardauskas@example.com', '2017-06-27 10:02:00', '2017-06-27 10:02:00'),
(3, 'Vardė Pavardė', '0037062589632', 'varde@example.com', '2017-06-27 10:02:08', '2017-06-27 10:02:08'),
(4, 'Vardenis Pavardenis', '861111111', 'vardenis@example.com', '2017-06-27 10:02:18', '2017-06-27 10:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hours` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `provider`, `address`, `phone`, `email`, `hours`, `created_at`, `updated_at`) VALUES
(1, 'UAB \"ASDF\"', 'Gatvės g. 5, Vilnius', '+37060000000', 'asdf@example.com', 'I-V 10:00 - 20:00', '2017-06-27 10:02:36', '2017-06-27 10:02:36'),
(2, 'UAB \"ASDFG\"', 'Gatvės g. 5, Vilnius', '+37060000000', 'asdfg@example.com', 'I-V 10:00 - 20:00', '2017-06-28 06:07:55', '2017-06-28 06:07:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(20, '2014_10_12_000000_create_users_table', 1),
(21, '2014_10_12_100000_create_password_resets_table', 1),
(22, '2017_06_21_200205_create_customers_table', 1),
(23, '2017_06_23_133100_create_contracts_table', 1),
(24, '2017_06_23_194622_create_details_table', 1),
(25, '2017_06_24_125155_create_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `device` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `defect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `employee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `device`, `serial`, `package`, `defect`, `priority`, `notes`, `employee`, `details_id`, `contract_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Samsung Galaxy s7', 'sn15481518', 'Telefonas', 'Neįsijungia', 'Neskubus', NULL, 'Vardžius Pavardžiūnas', 1, 11, '2017-06-27 10:03:31', '2017-06-28 06:11:24'),
(3, 2, 'iPad Pro', '464SDGT5604654', 'Planšetė, kroviklis', 'Nesikrauna', 'Vidutinis', NULL, 'Vardžius Pavardžiūnas', 1, 11, '2017-06-27 15:12:40', '2017-06-27 15:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jonusaitea@gmail.com', '$2y$10$fc5ri4T9rAGspzSxK/jfXOV1k49zxD0dHznKzZx0Vhc5fQsw7tFxi', '2017-07-13 05:31:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'jonusaitea@gmail.com', '$2y$10$HGL6X0WEEevYZme8AfvPnOpUL0FT2xDvcweN.UrUs.s5ULLxm.BUy', 'ojcVyfSJgsZBNnduu0Pqa0x4Ihpi6dspjyZayRmbAYwDqsz7EieJMLLsbRE6', '2017-06-27 10:01:32', '2017-06-28 04:56:41'),
(2, 'aiste', 'undinele1990@gmail.com', '$2y$10$/H.WwXZ8ZzNh.BLVEQ1eRORg63HgYuF2b6s0ugBcmmP7Q15T0nm22', 'zPapHmZuiaBTPOnvgIaaioV87BtDC9aEcmaH5bjK0AeJQoXjnDACtlF68XEK', '2017-06-28 04:52:01', '2017-06-28 04:52:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
