<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('notFound', function() {
   return view('notFound');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    // View profile
    Route::get('profile', 'ProfileController@profile')->name('profile');
    // Change password
    Route::post('changePassword', 'Auth\ChangePasswordController@changePassword')->name('changePassword');
    // Change e-mail
    Route::post('changeEmail', 'Auth\ChangeEmailController@changeEmail')->name('changeEmail');

    // View customers
    Route::get('customers', 'CustomerController@viewCustomers')->name('customers');
    // Add customer
    Route::get('newCustomer', 'CustomerController@newCustomer')->name('newCustomer');
    Route::post('addCustomer', 'CustomerController@addCustomer')->name('addCustomer');
    // Delete customer
    Route::get('deleteCustomer/{id}', 'CustomerController@deleteCustomer')->middleware('ajax');
    // Edit customer
    Route::get('editCustomer/{id}', 'CustomerController@editCustomer')->name('editCustomer');
    Route::post('saveCustomer/{id}', 'CustomerController@saveCustomer')->name('saveCustomer');

    // Edit contract
    Route::get('contract', 'ContractController@contract')->name('contract');
    Route::post('editContract','ContractController@editContract')->name('editContract');

    // Edit details
    Route::get('details', 'DetailsController@details')->name('details');
    Route::post('editDetails', 'DetailsController@editDetails')->name('editDetails');

    // New order
    Route::get('order', 'OrderController@order')->name('order');
    // Get customer phone and email
    Route::get('customerInfo/{id}', 'CustomerController@customerInfo')->middleware('ajax');
    // Save new order
    Route::post('newOrder', 'OrderController@newOrder')->name('newOrder');
    // Print saved order
    Route::get('print/{id}', 'OrderController@printPreview')->name('print');

    // View all orders list
    Route::get('orders', 'OrderController@orders')->name('orders');
    // Delete order
    Route::get('deleteOrder/{id}', 'OrderController@deleteOrder')->middleware('ajax');
    // Edit order
    Route::get('editOrder/{id}', 'OrderController@editOrder')->name('editOrder');
    Route::post('saveOrder/{id}', 'OrderController@saveOrder')->name('saveOrder');
    // Get customer phone and email
    Route::get('editOrder/customerInfo/{id}', 'CustomerController@customerInfo')->middleware('ajax');
    // Refresh contract
    Route::get('refreshContract', 'ContractController@refreshContract')->middleware('ajax');
    // Refresh details
    Route::get('refreshDetails', 'DetailsController@refreshDetails')->middleware('ajax');
    //Search
    Route::post('searchOrder', 'OrderController@searchOrder')->name('searchOrder');
});